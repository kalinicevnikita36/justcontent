---
title: 'The classification of basic types'
order: 3
---

In this lesson, we will learn about the classification and properties of the basic types provided by Kotlin.
You may have already met some of them. The basic types can be separated into several groups according to meaning.
Types from the same group can be operated in the same way, but have different sizes and, as a consequence,
represent different ranges of values.

You do not need to learn all of this by heart because it is easy to find it in the documentation or simply Google it.
Though, a common understanding of these concepts is important in job interviews and practice.

## Numbers

Kotlin provides several types for `integers` and `fractional` numbers.

Integer numbers (`0`, `1`, `2`, ...) are represented by the following four types: `Long`, `Int`, `Short`, `Byte`
(from the largest to the smallest). These types have different sizes and may represent different ranges of values.
The range of an integer type is calculated as −(2n−1) to (2n−1)−1, where n is the number of bits.
The range includes 0, which is the reason for subtracting 1 from the upper bound.

- `Byte` size 8 bits (1 byte), range from -128 to 127
- `Short` size 16 bits (2 bytes), range from -32768 to 32767
- `Int` size 32 bits (4 bytes), range from −(231) to (231)−1
- `Long` size 64 bits (8 bytes), range from −(263) to (263)−1

The sizes of types are always the same. They do not depend on the operating system or hardware and you cannot change them.

> The most commonly used integer types are `Int` and `Long`. Always try to use `Int` if it is enough for your purposes.
> Otherwise, use `Long`.

```kotlin
val zero = 0 // Int
val one = 1  // Int
val oneMillion = 1_000_000  // Int

val twoMillion = 2_000_000L // Long, because it is tagged with L
val bigNumber = 1_000_000_000_000_000 // Long, Kotlin automatically choose it (Int is too small)
val ten: Long = 10 // Long, because the type is specified

val shortNumber: Short = 15 // Short, because the type is specified
val byteNumber: Byte = 15   // Byte, because the type is specified
```

**Floating-point** types represent numbers with fractional parts. Kotlin has two such types: `Double` (64 bits) and
`Float` (32 bits). These types can store only a limited number of significant decimal digits (~6-7 for `Float` and
~14-16 for `Double`). As a rule, you will use the Double type in practice.

```kotlin
val pi = 3.1415  // Double
val e = 2.71828f // Float, because it is tagged with f
val fraction: Float = 1.51 // Float, because the type is specified
```

To display the maximum and minimum value of a numeric type (including `Double` and `Float`), you need to write the type
name followed by a dot and then either `MIN_VALUE` or `MAX_VALUE`.

```kotlin
println(Int.MIN_VALUE)  // -2147483648
println(Int.MAX_VALUE)  // 2147483647
println(Long.MIN_VALUE) // -9223372036854775808
println(Long.MAX_VALUE) // 9223372036854775807
```

It is also possible to get the size of an integer type in bytes or bits (1 byte = 8 bits):

```kotlin
println(Int.SIZE_BYTES) // 4
println(Int.SIZE_BITS)  // 32
```

## Characters

Kotlin has a type named `Char` to represent letters (uppercase and lowercase), digits, and other symbols.
Each character is just a single letter enclosed in single quotes. This type has the same size as the `Short` type
(2 bytes = 16 bits).

```kotlin
val lowerCaseLetter = 'a'
val upperCaseLetter = 'Q'
val number = '1'
val space = ' '
val dollar = '$'
```

Characters can represent symbols from many alphabets including hieroglyphs, as well as some special symbols that we
will take a look at later.

## Booleans

Kotlin provides a type called `Boolean`, which can store only two values: `true` and `false`. It represents only one
bit of information, but its size is not precisely defined.

```kotlin
val enabled = true
val bugFound = false
```

We will often use this type in conditionals.

## Strings

The `String` type represents a sequence of characters enclosed in double quotes. Actually, it is one of the most used types.

```kotlin
val creditCardNumber = "1234 5678 9012 3456"
val message = "Learn Kotlin instead of Java."
```

As a recap, note that the most commonly used of the types mentioned above are
`String`, `Int`, `Long`, `Boolean`, `Char`, and `Double`. We will learn about other data types later.
Remember that `Long` is the largest integer type. Knowing the sizes and ranges of integer types may help you get a job.
