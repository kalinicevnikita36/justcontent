---
title: 'Basic literals'
order: 2
---

Regardless of its complexity, basically a program performs operations on numbers, strings, and other values. These
values are called literals. Before we start writing the first programs, let's learn the basic literals in Kotlin:
integer numbers, characters, and strings. You can meet these literals everywhere in everyday life.

## Integer numbers

We use integer numbers to count things in the real world. We will also often use integer numbers in Kotlin.

Here are several examples of valid integer number literals separated by commas: `0`, `1`, `2`, `10`, `11`, `100`.

If an integer value contains a lot of digits, we can add underscores to divide the digits into blocks to make this
number more readable: for example, `1_000_000` is easier to read than the same value written as `1000000`.

## Characters

A single character can represent a digit, a letter, or another symbol. To write a single character, we wrap a symbol
in single quotes as follows: `'A'`, `'B'`, `'C'`, `'x'`, `'y'`, `'z'`, `'0'`, '1', '2', '9'. Character literals can
represent letters of an alphabet, digits from `'0'` to `'9'`, whitespaces (`' '`), or something else (`'$'`).

Do not confuse characters representing numbers (e.g. `'9'`), and numbers themselves (e.g., `9`).

A character cannot include two or more digits or letters, because it represents a single symbol. The next two examples
are **incorrect**: `'abc'`, `'543'` because these literals have too many characters.

## Strings

Strings represent text information such as a text of an advertisement, an address of a web page, or a login to a
website. A string is a sequence of any individual characters.

To write strings we wrap characters in double-quotes instead of single ones. Here are some valid examples: `"text"`,
`"I want to learn Kotlin"`, `"123456"`, `"e-mail@gmail.com"`. So, strings can include letters, digits, whitespaces and
other characters.

A string can also contain only a single character, like `"A"`. Do not confuse it with a character such as `'A'`, which
is not a string.

## Remember

Do not confuse these literals:

- `123` is an integer number, `"123"` is a string;
- `'A'` is a character, `"A"` is a string;
- `'1'` is a character, `1` is an integer number.

Kotlin has many other literals, and they are waiting for us.
