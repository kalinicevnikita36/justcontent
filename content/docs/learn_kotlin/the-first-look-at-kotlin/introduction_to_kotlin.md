---
title: 'Introduction to Kotlin'
order: 1
---

## What is Kotlin?

Kotlin is an effective, modern programming language developed by JetBrains. It has a clear and concise syntax that makes your code easy to read.

Kotlin is widely used in practice, and its popularity is constantly growing among developers around the world. Many developers who use Kotlin note that it makes development faster and more exciting :)

![Logo](./assets/logo.png)

Knowledge of Kotlin allows developers to write mobile, server-side and desktop applications, as well as useful frameworks, and libraries. As a general-purpose language, it can be used in many spheres such as the financial service industry, telecommunications, embedded systems, medicine, development tools (like IntelliJ IDEA), and so on.

## Multiplatform language

Kotlin is a multiplatform language.

- JVM: Kotlin is fully interoperable with Java, which means Kotlin works great with all existing Java code and libraries. It also allows companies to make a gradual migration from Java to Kotlin, because Java code can access Kotlin code too. At the same time, developers can use Kotlin as the only language for their projects without Java at all.
- Android: using Kotlin, you can create mobile applications for the most used operating system in the world.
- JS: Kotlin is also compatible with JavaScript, which enables you to develop client-side web applications and run them in a browser.
- Native: you can compile Kotlin to native binaries, and can be run under Windows, Linux, iOS, and MacOS.

![Kotlin - JVM & Android & JS & Native](./assets/image.png)

Among all these opportunities modern programmers prefer mobile and server-side development. Other areas are gaining popularity as well.

## Features

Kotlin is designed as a **pragmatic language**, which means, its main purpose is to solve real-world problems rather than completing research purposes.

It is also important that Kotlin supports multiple **programming paradigms**, such as imperative programming, object-oriented programming, generic programming, functional programming, and more. You will learn about these paradigms throughout the course.

Last but not least, Kotlin is a **tool-friendly language**, which means all popular development tools such as IntelliJ IDEA, Eclipse, and Android Studio are compatible with it.

## Short history

In July 2011, JetBrains unveiled the Kotlin Project, a new language for the Java Platform, which had been in development for a year. The name comes from Kotlin Island, near St. Petersburg, Russia. The primary goal of this project was to provide a safer and more concise alternative to Java in all the contexts where Java is currently used.

In 2016, the first official stable version was released (Kotlin v1.0). The developer community was already interested in using this language, especially on Android.

At the Google I/O 2017 conference, Google announced first-class support for Kotlin on Android. Chet Haase, the chief advocate for Android, said: "We understand that not everybody is on Kotlin right now, but we believe that you should get there".

At this moment, Kotlin is considered as a general-purpose language for many platforms, not only for Android. The language has several releases a year. The latest version can be found on the [official site](https://kotlinlang.org).

## The sample of Kotlin program

Here is a sample of a simple Kotlin program that prints `Hello, Kotlin!`. You may start it by clicking on the green triangle.

```kotlin
fun main() {
    println("Hello, Kotlin!")
}
```

For now, you do not need to understand how this code works, just enjoy it! :)
